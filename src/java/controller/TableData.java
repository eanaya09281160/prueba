/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.*;

/**
 *
 * @author dd
 */
@ManagedBean
@SessionScoped
public class TableData implements Serializable{
    
private static final Cuate [] cuates= new Cuate[]{
    new Cuate("unou","Uno",12345),
    new Cuate("dosd","Dos",50),
    new Cuate("trest","Tres",250)
    };

public Cuate[] getCuates(){
return cuates;
}

    /**
     * Creates a new instance of TableData
     */
    public TableData() {
    }
    
}
