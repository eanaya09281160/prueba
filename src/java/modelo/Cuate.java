/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author dd
 */
public class Cuate implements Serializable{
    private String Apellidos;
    private String Nombres;
    private double cuotas;
    
    public Cuate(){
        
    }

    public Cuate(String Apellidos, String Nombres, double cuotas) {
        this.Apellidos = Apellidos;
        this.Nombres = Nombres;
        this.cuotas = cuotas;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public double getCuotas() {
        return cuotas;
    }

    public void setCuotas(double cuotas) {
        this.cuotas = cuotas;
    }
    
}


